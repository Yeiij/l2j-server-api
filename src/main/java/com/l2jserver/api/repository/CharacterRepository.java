package com.l2jserver.api.repository;

import com.l2jserver.api.entity.Character;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CharacterRepository extends CrudRepository<Character, Integer> {
    @Query(value = "SELECT * FROM characters WHERE pkkills > 0 ORDER BY pkkills DESC LIMIT 10", nativeQuery = true)
    List<Character> topPk();

    @Query(value = "SELECT * FROM characters WHERE pvpkills > 0 ORDER BY pvpkills DESC LIMIT 10", nativeQuery = true)
    List<Character> topPvp();
}