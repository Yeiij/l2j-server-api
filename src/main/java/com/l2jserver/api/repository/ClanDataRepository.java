package com.l2jserver.api.repository;

import com.l2jserver.api.entity.ClanData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClanDataRepository extends CrudRepository<ClanData, Integer> {
    String topLevelQ = "SELECT * FROM clan_data ORDER BY clan_level DESC, reputation_score DESC LIMIT 10";

    @Query(value = topLevelQ, nativeQuery = true)
    List<ClanData> topLevel();
}