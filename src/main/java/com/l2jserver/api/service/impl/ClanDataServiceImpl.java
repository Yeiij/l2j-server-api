package com.l2jserver.api.service.impl;

import com.l2jserver.api.entity.ClanData;
import com.l2jserver.api.repository.ClanDataRepository;
import com.l2jserver.api.service.ClanDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClanDataServiceImpl implements ClanDataService {

    @Autowired
    ClanDataRepository clanDataRepository;

    @Override
    @Transactional(readOnly = true)
    public Iterable<ClanData> findAll() {
        return clanDataRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ClanData> findById(Integer id) {
        return clanDataRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ClanData> topLevel() {
        return clanDataRepository.topLevel();
    }
}
