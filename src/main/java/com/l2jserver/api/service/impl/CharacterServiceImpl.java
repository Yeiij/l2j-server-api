package com.l2jserver.api.service.impl;

import com.l2jserver.api.repository.CharacterRepository;
import com.l2jserver.api.service.CharacterService;
import com.l2jserver.api.entity.Character;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CharacterServiceImpl implements CharacterService {

    @Autowired
    private CharacterRepository characterRepository;

    @Override
    @Transactional(readOnly = true)
    public Iterable<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Character> findById(Integer id) {
        return characterRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Character> topPk() {
        return characterRepository.topPk();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Character> topPvp() {
        return characterRepository.topPvp();
    }
}
