package com.l2jserver.api.service;

import com.l2jserver.api.entity.Character;

import java.util.List;
import java.util.Optional;

public interface CharacterService {

    Iterable<Character> findAll();

    Optional<Character> findById(Integer id);

    List<Character> topPk();

    List<Character> topPvp();
}
