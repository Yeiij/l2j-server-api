package com.l2jserver.api.service;

import com.l2jserver.api.entity.ClanData;

import java.util.List;
import java.util.Optional;

public interface ClanDataService {

    Iterable<ClanData> findAll();

    Optional<ClanData> findById(Integer id);

    List<ClanData> topLevel();
}
