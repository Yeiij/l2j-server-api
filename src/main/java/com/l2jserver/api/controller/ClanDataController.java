package com.l2jserver.api.controller;

import com.l2jserver.api.entity.ClanData;
import com.l2jserver.api.entity.DTO.ClanDataDTO;
import com.l2jserver.api.service.ClanDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api/clan_data")
public class ClanDataController {

    @Autowired
    ClanDataService clanDataService;

    @GetMapping
    public List<ClanDataDTO> all() {
        List<ClanData> clanData = StreamSupport.stream(
                clanDataService.findAll().spliterator(), false
        ).collect(Collectors.toList());

        List<ClanDataDTO> clanDataDTO = new ArrayList<>();

        for (ClanData data: clanData) {
            clanDataDTO.add(data.getclanDataDTO());
        }

        return clanDataDTO;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> oneById(@PathVariable(value = "id") Integer id) {
        Optional<ClanData> clanData = clanDataService.findById(id);

        if (clanData.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        ClanDataDTO clanDataDTO = clanData.get().getclanDataDTO();

        return ResponseEntity.ok(clanDataDTO);
    }

    @GetMapping("/top-level")
    public List<ClanDataDTO> topLevel() {
        List<ClanData> clanData = clanDataService.topLevel();
        List<ClanDataDTO> clanDataDTO = new ArrayList<>();

        for (ClanData data: clanData) {
            clanDataDTO.add(data.getclanDataDTO());
        }

        return clanDataDTO;
    }

}
