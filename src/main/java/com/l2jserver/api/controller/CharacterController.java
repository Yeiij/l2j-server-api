package com.l2jserver.api.controller;

import com.l2jserver.api.entity.ClanData;
import com.l2jserver.api.entity.DTO.CharacterDTO;
import com.l2jserver.api.service.CharacterService;
import com.l2jserver.api.entity.Character;
import com.l2jserver.api.service.ClanDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api/characters")
public class CharacterController {

    @Autowired
    private ClanDataService clanDataService;
    @Autowired
    private CharacterService characterService;

    @GetMapping
    public List<CharacterDTO> all() {
        List<Character> character = StreamSupport.stream(
                characterService.findAll().spliterator(), false
        ).collect(Collectors.toList());

        List<CharacterDTO> characterDTO = new ArrayList<>();

        for (Character data : character) {
            characterDTO.add(data.getcharacterDTO());
        }

        for (CharacterDTO data : characterDTO) {
            if (data.getClanid() != null) {
                Optional<ClanData> clanData = clanDataService.findById(data.getClanid());

                if (!clanData.isEmpty()) {
                    data.setClan(clanData.get().getclanDataDTO());
                }
            }
        }

        return characterDTO;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> oneById(@PathVariable(value = "id") Integer id) {
        Optional<Character> character = characterService.findById(id);

        if (character.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        CharacterDTO characterDTO = character.get().getcharacterDTO();

        if (characterDTO.getClanid() != null) {
            Optional<ClanData> clanData = clanDataService.findById(characterDTO.getClanid());

            if (!clanData.isEmpty()) {
                characterDTO.setClan(clanData.get().getclanDataDTO());
            }
        }

        return ResponseEntity.ok(characterDTO);
    }

    @GetMapping("/top-pk")
    public List<CharacterDTO> topPk() {
        List<Character> character = characterService.topPk();
        List<CharacterDTO> characterDTO = new ArrayList<>();

        for (Character data : character) {
            characterDTO.add(data.getcharacterDTO());
        }

        for (CharacterDTO data : characterDTO) {
            if (data.getClanid() != null) {
                Optional<ClanData> clanData = clanDataService.findById(data.getClanid());

                if (!clanData.isEmpty()) {
                    data.setClan(clanData.get().getclanDataDTO());
                }
            }
        }

        return characterDTO;
    }

    @GetMapping("/top-pvp")
    public List<CharacterDTO> topPvp() {
        List<Character> character = characterService.topPvp();
        List<CharacterDTO> characterDTO = new ArrayList<>();

        for (Character data : character) {
            characterDTO.add(data.getcharacterDTO());
        }

        for (CharacterDTO data : characterDTO) {
            if (data.getClanid() != null) {
                Optional<ClanData> clanData = clanDataService.findById(data.getClanid());

                if (!clanData.isEmpty()) {
                    data.setClan(clanData.get().getclanDataDTO());
                }
            }
        }

        return characterDTO;
    }
}
