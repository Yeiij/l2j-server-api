package com.l2jserver.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class L2JServerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(L2JServerApiApplication.class, args);
	}

}
