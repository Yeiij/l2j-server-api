package com.l2jserver.api.entity;

import com.l2jserver.api.entity.DTO.ClanDataDTO;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "clan_data")
@Data
@Accessors(chain = true)
public class ClanData {

    @Id
    @Column(name = "clan_id", nullable = false)
    private Integer clanId;

    @Column(name = "clan_name")
    private String clanName;

    @Column(name = "clan_level")
    private Integer clanLevel;

    @Column(name = "reputation_score", nullable = false)
    private Integer reputationScore;

    @Column(name = "hasCastle")
    private Integer hasCastle;

    @Column(name = "blood_alliance_count", nullable = false)
    private Integer bloodAllianceCount;

    @Column(name = "blood_oath_count", nullable = false)
    private Integer bloodOathCount;

    @Column(name = "ally_id")
    private Integer allyId;

    @Column(name = "ally_name")
    private String allyName;

    @Column(name = "leader_id")
    private Integer leaderId;

    @Column(name = "crest_id")
    private Integer crestId;

    @Column(name = "crest_large_id")
    private Integer crestLargeId;

    @Column(name = "ally_crest_id")
    private Integer allyCrestId;

    @Column(name = "auction_bid_at", nullable = false)
    private Integer auctionBidAt;

    @Column(name = "ally_penalty_expiry_time", nullable = false)
    private Long allyPenaltyExpiryTime;

    @Column(name = "ally_penalty_type", nullable = false)
    private Integer allyPenaltyType;

    @Column(name = "char_penalty_expiry_time", nullable = false)
    private Long charPenaltyExpiryTime;

    @Column(name = "dissolving_expiry_time", nullable = false)
    private Long dissolvingExpiryTime;

    @Column(name = "new_leader_id", nullable = false)
    private Integer newLeaderId;

    public ClanDataDTO getclanDataDTO(){
        return new ClanDataDTO(
                this.clanName,
                this.clanLevel,
                this.reputationScore,
                this.hasCastle,
                this.allyName
        );
    }

}
