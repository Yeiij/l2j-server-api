package com.l2jserver.api.entity.DTO;

public class CharacterDTO {
    private final String[] CLASS_CHOICES = new String[]{
            "fighter",
            "warrior",
            "gladiator",
            "warlord",
            "knight",
            "paladin",
            "darkAvenger",
            "rogue",
            "treasureHunter",
            "hawkeye",
            "mage",
            "wizard",
            "sorceror",
            "necromancer",
            "warlock",
            "cleric",
            "bishop",
            "prophet",
            "elvenFighter",
            "elvenKnight",
            "templeKnight",
            "swordSinger",
            "elvenScout",
            "plainsWalker",
            "silverRanger",
            "elvenMage",
            "elvenWizard",
            "spellsinger",
            "elementalSummoner",
            "oracle",
            "elder",
            "darkFighter",
            "palusKnight",
            "shillienKnight",
            "bladedancer",
            "assassin",
            "abyssWalker",
            "phantomRanger",
            "darkMage",
            "darkWizard",
            "spellhowler",
            "phantomSummoner",
            "shillienOracle",
            "shillenElder",
            "orcFighter",
            "orcRaider",
            "destroyer",
            "orcMonk",
            "tyrant",
            "orcMage",
            "orcShaman",
            "overlord",
            "warcryer",
            "dwarvenFighter",
            "scavenger",
            "bountyHunter",
            "artisan",
            "warsmith",
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            "duelist",
            "dreadnought",
            "phoenixKnight",
            "hellKnight",
            "sagittarius",
            "adventurer",
            "archmage",
            "soultaker",
            "arcanaLord",
            "cardinal",
            "hierophant",
            "evaTemplar",
            "swordMuse",
            "windRider",
            "moonlightSentinel",
            "mysticMuse",
            "elementalMaster",
            "evaSaint",
            "shillienTemplar",
            "spectralDancer",
            "ghostHunter",
            "ghostSentinel",
            "stormScreamer",
            "spectralMaster",
            "shillienSaint",
            "titan",
            "grandKhavatari",
            "dominator",
            "doomcryer",
            "fortuneSeeker",
            "maestro",
            "dummyEntry31",
            "dummyEntry32",
            "dummyEntry33",
            "dummyEntry34",
            "maleSoldier",
            "femaleSoldier",
            "trooper",
            "warder",
            "berserker",
            "maleSoulbreaker",
            "femaleSoulbreaker",
            "arbalester",
            "doombringer",
            "maleSoulhound",
            "femaleSoulhound",
            "trickster",
            "inspector",
            "judicator"
    };

    private final String[] RACE_CHOICES = new String[]{
            "human",
            "elf",
            "dark elf",
            "orc",
            "dwarf",
            "kamael",
            "animal",
            "beast",
            "bug",
            "castle guard",
            "construct",
            "demonic",
            "divine",
            "dragon",
            "elemental",
            "etc",
            "fairy",
            "giant",
            "humanoid",
            "mercenary",
            null,
            "plant",
            "siege weapon",
            "undead"
    };

    private final String[] SEX_CHOICES = new String[]{
            "male",
            "female",
            "other"
    };

    private final String charName;
    private final Integer classid;
    private final Integer fame;
    private final Integer karma;
    private final Integer level;
    private final Integer nobless;
    private final Integer pvpkills;
    private final Integer pkkills;
    private final Integer race;
    private final Integer sex;
    private final Integer clanid;
    private ClanDataDTO clan;

    public CharacterDTO(String charName, Integer classid, Integer fame, Integer karma, Integer level, Integer nobless, Integer pvpkills, Integer pkkills, Integer race, Integer sex, Integer clanid) {
        this.charName = charName;
        this.classid = classid;
        this.fame = fame;
        this.karma = karma;
        this.level = level;
        this.nobless = nobless;
        this.pvpkills = pvpkills;
        this.pkkills = pkkills;
        this.race = race;
        this.sex = sex;
        this.clanid = clanid;
    }


    public String getCharName() {
        return charName;
    }

    public String getClassid() {
        return CLASS_CHOICES[classid];
    }

    public Integer getFame() {
        return fame;
    }

    public Integer getKarma() {
        return karma;
    }

    public Integer getLevel() {
        return level;
    }

    public Integer getNobless() {
        return nobless;
    }

    public Integer getPvpkills() {
        return pvpkills;
    }

    public Integer getPkkills() {
        return pkkills;
    }

    public String getRace() {
        return RACE_CHOICES[race];
    }

    public String getSex() {
        return SEX_CHOICES[sex];
    }

    public Integer getClanid() {
        return clanid;
    }

    public ClanDataDTO getClan() {
        return clan;
    }

    public void setClan(ClanDataDTO clanDataDTO) {
        this.clan = clanDataDTO;
    }
}
