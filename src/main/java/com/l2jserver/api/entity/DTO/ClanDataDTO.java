package com.l2jserver.api.entity.DTO;

public class ClanDataDTO {

    private final String clanName;
    private final Integer clanLevel;
    private final Integer reputationScore;
    private final Integer hasCastle;
    private final String allyName;

    public ClanDataDTO(String clanName, Integer clanLevel, Integer reputationScore, Integer hasCastle, String allyName) {
        this.clanName = clanName;
        this.clanLevel = clanLevel;
        this.reputationScore = reputationScore;
        this.hasCastle = hasCastle;
        this.allyName = allyName;
    }

    public String getClanName() {
        return clanName;
    }

    public String getClanLevel() {
        return clanLevel.toString();
    }

    public String getReputationScore() {
        return reputationScore.toString();
    }

    public String getHasCastle() {
        return (this.hasCastle == 0) ? "no" : "yes";
    }

    public String getAllyName() {
        return (this.allyName == null) ? "" : this.allyName;
    }

}
