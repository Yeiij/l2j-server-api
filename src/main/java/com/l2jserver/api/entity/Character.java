package com.l2jserver.api.entity;

import com.l2jserver.api.entity.DTO.CharacterDTO;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "characters")
@Data
@Accessors(chain = true)
public class Character {

    @Column(name = "account_name")
    private String accountName;

    @Id
    @Column(name = "charId", nullable = false)
    private Integer charId;

    @Column(name = "char_name", nullable = false)
    private String charName;

    @Column(name = "level")
    private Integer level;

    @Column(name = "maxHp")
    private Integer maxHp;

    @Column(name = "curHp")
    private Integer curHp;

    @Column(name = "maxCp")
    private Integer maxCp;

    @Column(name = "curCp")
    private Integer curCp;

    @Column(name = "maxMp")
    private Integer maxMp;

    @Column(name = "curMp")
    private Integer curMp;

    @Column(name = "face")
    private Integer face;

    @Column(name = "hairStyle")
    private Integer hairStyle;

    @Column(name = "hairColor")
    private Integer hairColor;

    @Column(name = "sex")
    private Integer sex;

    @Column(name = "heading")
    private Integer heading;

    @Column(name = "x")
    private Integer x;

    @Column(name = "y")
    private Integer y;

    @Column(name = "z")
    private Integer z;

    @Column(name = "exp")
    private Long exp;

    @Column(name = "expBeforeDeath")
    private Long expBeforeDeath;

    @Column(name = "sp", nullable = false)
    private Integer sp;

    @Column(name = "karma")
    private Integer karma;

    @Column(name = "fame", nullable = false)
    private Integer fame;

    @Column(name = "pvpkills")
    private Integer pvpkills;

    @Column(name = "pkkills")
    private Integer pkkills;

    @Column(name = "clanid")
    private Integer clanid;

    @Column(name = "race")
    private Integer race;

    @Column(name = "classid")
    private Integer classid;

    @Column(name = "base_class", nullable = false)
    private Integer baseClass;

    @Column(name = "transform_id", nullable = false)
    private Integer transformId;

    @Column(name = "deletetime", nullable = false)
    private Long deletetime;

    @Column(name = "cancraft")
    private Integer cancraft;

    @Column(name = "title")
    private String title;

    @Column(name = "title_color", nullable = false)
    private Integer titleColor;

    @Column(name = "accesslevel")
    private Integer accesslevel;

    @Column(name = "online")
    private Integer online;

    @Column(name = "onlinetime")
    private Integer onlinetime;

    @Column(name = "char_slot")
    private Integer charSlot;

    @Column(name = "newbie")
    private Integer newbie;

    @Column(name = "lastAccess", nullable = false)
    private Long lastAccess;

    @Column(name = "clan_privs")
    private Integer clanPrivs;

    @Column(name = "wantspeace")
    private Integer wantspeace;

    @Column(name = "isin7sdungeon", nullable = false)
    private Integer isin7sdungeon;

    @Column(name = "power_grade")
    private Integer powerGrade;

    @Column(name = "nobless", nullable = false)
    private Integer nobless;

    @Column(name = "subpledge", nullable = false)
    private Integer subpledge;

    @Column(name = "lvl_joined_academy", nullable = false)
    private Integer lvlJoinedAcademy;

    @Column(name = "apprentice", nullable = false)
    private Integer apprentice;

    @Column(name = "sponsor", nullable = false)
    private Integer sponsor;

    @Column(name = "clan_join_expiry_time", nullable = false)
    private Long clanJoinExpiryTime;

    @Column(name = "clan_create_expiry_time", nullable = false)
    private Long clanCreateExpiryTime;

    @Column(name = "death_penalty_level", nullable = false)
    private Integer deathPenaltyLevel;

    @Column(name = "bookmarkslot", nullable = false)
    private Integer bookmarkslot;

    @Column(name = "vitality_points", nullable = false)
    private Integer vitalityPoints;

    @Column(name = "createDate", nullable = false)
    private Timestamp createDate;

    @Column(name = "language")
    private String language;

    public CharacterDTO getcharacterDTO(){
        return new CharacterDTO(
                this.charName,
                this.classid,
                this.fame,
                this.karma,
                this.level,
                this.nobless,
                this.pvpkills,
                this.pkkills,
                this.race,
                this.sex,
                this.clanid
        );
    }

}
