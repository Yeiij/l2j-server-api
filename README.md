# L2JServer API
API implementation for [L2JServer](https://www.l2jserver.com/)
 
+ [OpenJDK 15](https://openjdk.java.net/projects/jdk/15/)  
+ [Spring Boot](https://spring.io/)  
+ [MariaDB](https://mariadb.org/)  


## Configurations
### Application Properties
```Path
src/main/resources/application.properties
```

#### Database Configuration
```Properties
# Database Configuration
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver
spring.datasource.url=jdbc:mariadb://127.0.0.1:3306/l2jgs
spring.datasource.username=l2j
spring.datasource.password=l2jserver2019

# Allows Hibernate to generate SQL optimized for a particular DBMS
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MariaDBDialect
spring.jpa.database-platform=org.hibernate.dialect.MariaDBDialect
```

#### Spring Server Configuration
```Properties
# Spring Server setup
server.address=127.0.0.1
server.port=8080
server.compression.enabled=true
server.http2.enabled=true
```

#### Spring Security Configuration
```Properties
# Spring Security Configuration
spring.security.user.name=l2j
spring.security.user.password=l2jserver2019
```


## Compile
`mvn clean package`


## Run
`java -jar target/l2j-server-api-0.0.1-SNAPSHOT.jar`


## API URL's


### Player Related
[TOP **PK** URL](http://127.0.0.1:8080/api/characters/top-pk) `http://127.0.0.1:8080/api/characters/top-pk`  
[TOP **PVP** URL](http://127.0.0.1:8080/api/characters/top-pvp) `http://127.0.0.1:8080/api/characters/top-pvp`


### Clan Related
[TOP **Level** URL](http://127.0.0.1:8080/api/clan_data/top-level) `http://127.0.0.1:8080/api/clan_data/top-level`  
